'use strict';

($ => {

  const app = {

    init: () => {

      app.interactive();

      $(window).resize(() => {
        app.resizeEvents();
      }).resize();

      $(window).scroll(() => {
        app.scrollEvents();
      }).scroll();


		$('.faq__headline').on('click', function (e) {
		  e.preventDefault();
		  if ($(this).hasClass('opened')) {
				$('.faq__headline').removeClass('opened');
				$('.faq__item').removeClass('opened');
			} else {
				$('.faq__headline').removeClass('opened');
				$('.faq__item').removeClass('opened');
				$(this).closest('.faq__headline').addClass('opened');
				$(this).closest('.faq__item').addClass('opened');
			}
	  });

		$('.layer-1 .btn--cta').on('click', function (e) {
			e.preventDefault();
			$('.layer-1').addClass('hidden');
			$('.layer-2').addClass('visible');
		});

		$('.layer-2__item--top, .layer-2__item--bot').on('click', function (e) {
			$(this).addClass('reverted');
	  });

	  $('.tabs__img').on('click', function (e) {
		  $(this).addClass('hidden');
		  $('.tabs__text').addClass('visible');
	  });

	  $('.tabs__checkbox').on('click', function (e) {
		  $('.tabs__img').removeClass('hidden');
		  $('.tabs__text').removeClass('visible');
	  });

	  $(".tabs").tabs({
		  show: {
			  effect: "fadeIn",
			  duration: 800
		  }
	  });

	  $(".contacts__form-inp, .contacts__form-text").focus(function () {
		$(this).prev('.contacts__form-label').addClass('red');
	  });

	  $(".contacts__form-inp, .contacts__form-text").blur(function () {
		  $(this).prev('.contacts__form-label').removeClass('red');
	  });

		$('.menu-burger').on('click', function () {
			$('.menu-burger').toggleClass('is-active');
			$('body').toggleClass('menu-opened');
		});

		$('.header__logo').on('click', function () {
			if($('body').hasClass('menu-opened')) {
				$('body').removeClass('menu-opened');
				$('.menu-burger').removeClass('is-active');
			}
		});

		function findGetParameter(parameterName) {
			let result = null,
					tmp = [];
			let items = location.search.substr(1).split("&");
			for (let index = 0; index < items.length; index++) {
				tmp = items[index].split("=");
				if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
			}
			return result;
		}

		let tab = findGetParameter('tab');

		if (tab) {
			openTab(tab);
		}

		function openTab(tab) {
			if (tab) {

				$('.faq__item').each(function () {
					let $el = $(this);

					if ( $el.hasClass(tab) ) {
						let $elLink = $el.find('.faq__headline');

						$el.addClass('opened');
						$elLink.addClass('opened');
					}
				});
			}
		}

		},

    interactive: () => {

    },

    resizeEvents: () => {
			if($(window).width()< 768){
				$('.scene--front').on('click', function (e) {
					$(this).find('img').addClass('hide');
					$(this).find('.m-visible').addClass('show');
				});
			}
		},

    scrollEvents: () => {

    }
  };

  $(document).ready(() => {
    app.init();
  });

})(jQuery);